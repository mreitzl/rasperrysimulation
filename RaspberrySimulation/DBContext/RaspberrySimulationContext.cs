﻿using Microsoft.EntityFrameworkCore;
using RaspberrySimulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RaspberrySimulation.DBContext
{
    public class RaspberrySimulationContext : DbContext
    {
        public RaspberrySimulationContext(DbContextOptions<RaspberrySimulationContext> options) : base(options) { }

        public DbSet<MobilePhone> MobilePhone { get; set; }
        public DbSet<MobilePhoneData> MobilePhoneData { get; set; }

        public DbSet<TrafficJam> TrafficJam { get; set; }
    }
}
