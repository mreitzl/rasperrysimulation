﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RaspberrySimulation.APIModels
{
    public class TrafficJamDataModel
    {
        public int Id { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public int DurationInSeconds { get; set; }
    }
}
