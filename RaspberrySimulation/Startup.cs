using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RaspberrySimulation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RaspberrySimulation.DBContext;
using RaspberrySimulation.Services;
using Microsoft.Data.SqlClient;

namespace RaspberrySimulation
{
    public class Startup
    {
        private readonly string apiKey = "thisIsATestApiKeyForAStudentProject";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var builder = new SqlConnectionStringBuilder(Configuration.GetConnectionString("RaspberryPiContext"));
            //builder.UserID = Configuration["DbUser"];
            //builder.Password = Configuration["DbPassword"];
            builder.UserID = "master";
            builder.Password = "password";
            services.AddDbContext<RaspberrySimulationContext>(options => options.UseSqlServer(builder.ConnectionString,
                optionsAction => optionsAction.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery)));
            services.AddControllersWithViews();
            services.AddHostedService<RaspiService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseWhen(context => context.Request.Path.StartsWithSegments(
                MobilePhoneAPIController.APIROUTE,
                StringComparison.OrdinalIgnoreCase), 
                appBuilder => appBuilder.UseMiddleware<APIAccessMiddleware>(apiKey));

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
