﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RaspberrySimulation.Migrations
{
    public partial class AddTrafficJam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TrafficJamID",
                table: "MobilePhone",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "TrafficJam",
                columns: table => new
                {
                    TrafficJamID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Longitude = table.Column<double>(type: "float", nullable: false),
                    Latitude = table.Column<double>(type: "float", nullable: false),
                    DurationInSeconds = table.Column<double>(type: "float", nullable: false),
                    IsReported = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrafficJam", x => x.TrafficJamID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MobilePhone_TrafficJamID",
                table: "MobilePhone",
                column: "TrafficJamID");

            migrationBuilder.AddForeignKey(
                name: "FK_MobilePhone_TrafficJam_TrafficJamID",
                table: "MobilePhone",
                column: "TrafficJamID",
                principalTable: "TrafficJam",
                principalColumn: "TrafficJamID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MobilePhone_TrafficJam_TrafficJamID",
                table: "MobilePhone");

            migrationBuilder.DropTable(
                name: "TrafficJam");

            migrationBuilder.DropIndex(
                name: "IX_MobilePhone_TrafficJamID",
                table: "MobilePhone");

            migrationBuilder.DropColumn(
                name: "TrafficJamID",
                table: "MobilePhone");
        }
    }
}
