﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RaspberrySimulation.Migrations
{
    public partial class TrafficJam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DurationInSeconds",
                table: "TrafficJam");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "TrafficJam");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "TrafficJam");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "DurationInSeconds",
                table: "TrafficJam",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "TrafficJam",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "TrafficJam",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
