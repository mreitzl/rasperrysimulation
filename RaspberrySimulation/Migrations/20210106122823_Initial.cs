﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RaspberrySimulation.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MobilePhone",
                columns: table => new
                {
                    MobilePhoneID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MobilePhoneHash = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MobilePhone", x => x.MobilePhoneID);
                });

            migrationBuilder.CreateTable(
                name: "MobilePhoneData",
                columns: table => new
                {
                    MobilePhoneDataID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MobilePhoneID = table.Column<int>(type: "int", nullable: false),
                    Longitude = table.Column<double>(type: "float", nullable: false),
                    Latitude = table.Column<double>(type: "float", nullable: false),
                    DurationInSeconds = table.Column<int>(type: "int", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MobilePhoneData", x => x.MobilePhoneDataID);
                    table.ForeignKey(
                        name: "FK_MobilePhoneData_MobilePhone_MobilePhoneID",
                        column: x => x.MobilePhoneID,
                        principalTable: "MobilePhone",
                        principalColumn: "MobilePhoneID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MobilePhoneData_MobilePhoneID",
                table: "MobilePhoneData",
                column: "MobilePhoneID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MobilePhoneData");

            migrationBuilder.DropTable(
                name: "MobilePhone");
        }
    }
}
