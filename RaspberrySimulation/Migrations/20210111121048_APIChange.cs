﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RaspberrySimulation.Migrations
{
    public partial class APIChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DurationInSeconds",
                table: "MobilePhoneData");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DurationInSeconds",
                table: "MobilePhoneData",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
