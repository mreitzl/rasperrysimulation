﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RaspberrySimulation.Migrations
{
    public partial class TrafficJamNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MobilePhone_TrafficJam_TrafficJamID",
                table: "MobilePhone");

            migrationBuilder.AlterColumn<int>(
                name: "TrafficJamID",
                table: "MobilePhone",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_MobilePhone_TrafficJam_TrafficJamID",
                table: "MobilePhone",
                column: "TrafficJamID",
                principalTable: "TrafficJam",
                principalColumn: "TrafficJamID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MobilePhone_TrafficJam_TrafficJamID",
                table: "MobilePhone");

            migrationBuilder.AlterColumn<int>(
                name: "TrafficJamID",
                table: "MobilePhone",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MobilePhone_TrafficJam_TrafficJamID",
                table: "MobilePhone",
                column: "TrafficJamID",
                principalTable: "TrafficJam",
                principalColumn: "TrafficJamID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
