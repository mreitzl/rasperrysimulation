﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RaspberrySimulation.APIModels;
using RaspberrySimulation.DBContext;
using RaspberrySimulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RaspberrySimulation.Services
{
    public class RaspiService : BackgroundService
    {
        private readonly TimeSpan ExecutionDelay = TimeSpan.FromSeconds(30);

        private readonly IServiceScopeFactory serviceScopeFactory;

        static HttpService _httpservices = new HttpService();

        public RaspiService(IServiceScopeFactory serviceScopeFactory)
        {
            this.serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(ExecutionDelay, stoppingToken).ConfigureAwait(true);

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    Execute(stoppingToken);
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc);
                }
                finally
                {
                    await Task.Delay(ExecutionDelay, stoppingToken).ConfigureAwait(true);
                }
            }
        }

        private async void Execute(CancellationToken stoppingToken)
        {
            using var scope = serviceScopeFactory.CreateScope();
            var context = scope.ServiceProvider.GetService<RaspberrySimulationContext>();
            foreach (var mobilePhone in context.MobilePhone.Include(x => x.MobilePhoneData).AsEnumerable().ToList())
            {
                if (stoppingToken.IsCancellationRequested)
                {
                    return;
                }
                
                if ((DateTime.UtcNow - mobilePhone.Timestamp).TotalSeconds > 300)
                {
                    context.MobilePhone.Remove(mobilePhone);
                    context.SaveChanges();
                }
                else
                {
                    var lastActivity = mobilePhone.MobilePhoneData.OrderBy(x => x.Timestamp).FirstOrDefault();
                    if (lastActivity != null)
                    {
                        var found = false;
                        foreach (var trafficJam in context.TrafficJam.AsEnumerable())
                        {
                            var averageLongitude = Enumerable.Average(trafficJam.MobilePhones.Select(x => x.MobilePhoneData.OrderBy(t => t.Timestamp).FirstOrDefault().Longitude));
                            var averageLatitude = Enumerable.Average(trafficJam.MobilePhones.Select(x => x.MobilePhoneData.OrderBy(t => t.Timestamp).FirstOrDefault().Latitude));
                            if (CalculateDistanceInMeters(averageLatitude, averageLongitude, lastActivity.Latitude, lastActivity.Longitude) < 50)
                            {
                                found = true;
                                context.TrafficJam.Find(trafficJam.TrafficJamID).MobilePhones.Add(mobilePhone);
                            }
                        }
                        if (!found)
                        {
                            context.TrafficJam.Add(new TrafficJam
                            {
                                IsReported = false,
                                MobilePhones = new List<MobilePhone> { lastActivity.MobilePhone }
                            });
                        }

                        context.SaveChanges();
                    }
                }
            }

            foreach (var trafficJam in context.TrafficJam.Include(x => x.MobilePhones).ThenInclude(y => y.MobilePhoneData).AsEnumerable().ToList())
            {
                if (stoppingToken.IsCancellationRequested)
                {
                    return;
                }

                if (trafficJam.MobilePhones.Any())
                {
                    if (trafficJam.IsReported)
                    {
                        if (trafficJam.MobilePhones.Count() < 1)
                        {
                            var statusCode = await _httpservices.DeleteRequest(trafficJam.TrafficJamID);
                            if (!(statusCode == System.Net.HttpStatusCode.OK))
                            {
                                Console.WriteLine("Update Failed: " + statusCode);
                            }
                            else
                            {
                                trafficJam.IsReported = false;
                            }
                        }
                        else
                        {
                            var trafficJamModel = new TrafficJamDataModel
                            {
                                Id = trafficJam.TrafficJamID,
                                Longitude = Enumerable.Average(trafficJam.MobilePhones.Select(x => x.MobilePhoneData.OrderBy(t => t.Timestamp).FirstOrDefault().Longitude)),
                                Latitude = Enumerable.Average(trafficJam.MobilePhones.Select(x => x.MobilePhoneData.OrderBy(t => t.Timestamp).FirstOrDefault().Latitude)),
                                DurationInSeconds = (int)Enumerable.Average(trafficJam.MobilePhones.Select(x => (DateTime.UtcNow - x.MobilePhoneData.OrderByDescending(t => t.Timestamp).FirstOrDefault().Timestamp).TotalSeconds))
                            };
                            var statusCode = await _httpservices.PutRequest(trafficJamModel);
                            if (!(statusCode == System.Net.HttpStatusCode.OK))
                            {
                                Console.WriteLine("Update Failed: " + statusCode);
                            }
                        }
                    }
                    else
                    {
                        if (trafficJam.MobilePhones.Count() > 0)
                        {
                            var trafficJamModel = new TrafficJamDataModel
                            {
                                Id = trafficJam.TrafficJamID,
                                Longitude = Enumerable.Average(trafficJam.MobilePhones.Select(x => x.MobilePhoneData.OrderBy(t => t.Timestamp).FirstOrDefault().Longitude)),
                                Latitude = Enumerable.Average(trafficJam.MobilePhones.Select(x => x.MobilePhoneData.OrderBy(t => t.Timestamp).FirstOrDefault().Latitude)),
                                DurationInSeconds = (int)Enumerable.Average(trafficJam.MobilePhones.Select(x => (DateTime.UtcNow - x.MobilePhoneData.OrderByDescending(t => t.Timestamp).FirstOrDefault().Timestamp).TotalSeconds))
                            };
                            var statusCode = await _httpservices.PostRequest(trafficJamModel);
                            if (!(statusCode == System.Net.HttpStatusCode.OK))
                            {
                                Console.WriteLine("Update Failed " + statusCode);
                            }
                            else
                            {
                                trafficJam.IsReported = true;
                            }

                        }
                    }
                    context.SaveChanges();
                }
                else
                {
                    if (trafficJam.IsReported)
                    {
                        var statusCode = await _httpservices.DeleteRequest(trafficJam.TrafficJamID);
                        if (!(statusCode == System.Net.HttpStatusCode.OK))
                        {
                            Console.WriteLine("Update Failed " + statusCode);
                        }
                        else
                        {
                            trafficJam.IsReported = false;
                        }
                    }
                    context.TrafficJam.Remove(trafficJam);
                    context.SaveChanges();
                }
            }
        }

        private double CalculateDistanceInMeters(double latitudePoint1, double longitudePoint1, double latitudePoint2, double longitudePoint2)
        {
            var d1 = latitudePoint1 * (Math.PI / 180.0);
            var num1 = longitudePoint1 * (Math.PI / 180.0);
            var d2 = latitudePoint2 * (Math.PI / 180.0);
            var num2 = longitudePoint2 * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                     Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }
    }
}
