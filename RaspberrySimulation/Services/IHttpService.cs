﻿using RaspberrySimulation.APIModels;
using RaspberrySimulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RaspberrySimulation.Services
{
    public interface IHttpService
    {
        Task<HttpStatusCode> PostRequest(TrafficJamDataModel trafficjammodel);
        Task<HttpStatusCode> PutRequest(TrafficJamDataModel trafficjammodel);
        Task<HttpStatusCode> DeleteRequest(int Id);
        
    }
}
