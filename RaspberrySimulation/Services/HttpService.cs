﻿using RaspberrySimulation.APIModels;
using RaspberrySimulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace RaspberrySimulation.Services
{
    public class HttpService : IHttpService
    {
        private HttpClient _client = new HttpClient();
        private string apiBaseUrl = "http://ec2-34-204-10-234.compute-1.amazonaws.com/api/v1/";

        public async Task<HttpStatusCode> PostRequest(TrafficJamDataModel trafficjammodel)
        {
            var jsonString = JsonSerializer.Serialize(trafficjammodel);
            var requestContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync($"{apiBaseUrl}trafficjam", requestContent);
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> PutRequest(TrafficJamDataModel trafficjammodel)
        {
            var jsonString = JsonSerializer.Serialize(trafficjammodel);
            var requestContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync($"{apiBaseUrl}trafficjam/{trafficjammodel.Id}", requestContent);
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> DeleteRequest(int id)
        {
            var response = await _client.DeleteAsync($"{apiBaseUrl}trafficjam/{id}");
            return response.StatusCode;
        }
    }
}
