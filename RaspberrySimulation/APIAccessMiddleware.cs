﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RaspberrySimulation
{
    public class APIAccessMiddleware
    {
        private readonly string apiKey;
        private readonly RequestDelegate next;

        public APIAccessMiddleware(RequestDelegate next, string apiKey)
        {
            this.next = next;
            this.apiKey = apiKey;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (!httpContext.Request.Headers.TryGetValue("X-API-KEY", out var requestApiKey) || string.IsNullOrEmpty(requestApiKey)) 
            {
                httpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                await httpContext.Response.WriteAsync("Missing Api Key.");
            }
            else if (requestApiKey == apiKey)
            {
                await next(httpContext).ConfigureAwait(false);
            }
            else
            {
                httpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                await httpContext.Response.WriteAsync("Wrong Api Key.");
            }
        }

    }
}
