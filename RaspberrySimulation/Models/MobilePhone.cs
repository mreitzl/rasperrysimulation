﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RaspberrySimulation.Models
{
    public class MobilePhone
    {
        public int MobilePhoneID { get; set; }

        public string MobilePhoneHash { get; set; }

        public ICollection<MobilePhoneData> MobilePhoneData { get; set;}

        public int? TrafficJamID { get; set; }

        public TrafficJam TrafficJam { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
