﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RaspberrySimulation.Models
{
    public class MobilePhoneData
    {
        public int MobilePhoneDataID { get; set; }

        public int MobilePhoneID { get; set; }

        public MobilePhone MobilePhone { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
