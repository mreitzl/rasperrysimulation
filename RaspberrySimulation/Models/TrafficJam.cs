﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RaspberrySimulation.Models
{
    public class TrafficJam
    {
        public int TrafficJamID { get; set; }

        public ICollection<MobilePhone> MobilePhones { get; set; }

        public bool IsReported { get; set; }
    }
}
