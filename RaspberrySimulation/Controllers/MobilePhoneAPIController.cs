﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RaspberrySimulation.APIModels;
using RaspberrySimulation.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RaspberrySimulation.Controllers
{
    [Route(APIROUTE)]
    [ApiController]
    public class MobilePhoneAPIController : Controller
    {
        public const string APIROUTE = "/API/V1";
        private readonly RaspberrySimulationContext context;

        public MobilePhoneAPIController(RaspberrySimulationContext context)
        {
            this.context = context;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(APIROUTE + "/MobilePhoneData")]
        public IActionResult PostMobilePhoneData([FromBody] MobilePhoneDataModel mobilePhoneModel)
        {
            if (context.MobilePhone.Where(f => f.MobilePhoneHash == mobilePhoneModel.MobilePhoneId).Count() != 1)
            {
                return BadRequest("Mobile Phone ID not found.");
            }

            if (mobilePhoneModel.Latitude > 90 || mobilePhoneModel.Latitude < -90)
            {
                return BadRequest("Check Latitude value.");
            }

            if (mobilePhoneModel.Longitude > 180 || mobilePhoneModel.Longitude < -180)
            {
                return BadRequest("Check Longitude value.");
            }

            context.MobilePhoneData.Add(new Models.MobilePhoneData
            {
                Latitude = mobilePhoneModel.Latitude,
                Longitude = mobilePhoneModel.Longitude,
                Timestamp = DateTime.UtcNow,
                MobilePhone = context.MobilePhone.Where(f => f.MobilePhoneHash == mobilePhoneModel.MobilePhoneId).FirstOrDefault(),
            });

            context.MobilePhone.Where(f => f.MobilePhoneHash == mobilePhoneModel.MobilePhoneId).FirstOrDefault().Timestamp = DateTime.UtcNow;
            context.SaveChanges();
            return Ok();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route(APIROUTE + "/MobilePhoneId")]
        public IActionResult GetMobilePhoneId()
        {
            var hash = GenerateHash();
            while (context.MobilePhone.Where(f => f.MobilePhoneHash == hash).Any())
            {
                hash = GenerateHash();
            }

            context.MobilePhone.Add(new Models.MobilePhone { MobilePhoneHash = hash, Timestamp = DateTime.UtcNow });
            context.SaveChanges();
            return Ok(hash);
        }

        [AllowAnonymous]
        [HttpDelete]
        [Route(APIROUTE + "/MobilePhoneId/{mobilePhoneId}")]
        public IActionResult DeleteMobilePhoneId([FromRoute] string mobilePhoneId)
        {
            var mobilePhone = context.MobilePhone.Where(f => f.MobilePhoneHash == mobilePhoneId);
            if (mobilePhone.Count() != 1)
            {
                return BadRequest("Mobile Phone ID not found.");
            }

            context.MobilePhone.Remove(mobilePhone.FirstOrDefault());
            context.SaveChanges();
            return Ok();
        }

        public string GenerateHash()
        {
            using var algorithm = SHA512.Create();
            var random = new Random();
            var randInt = random.Next();
            var datetimenow = DateTime.UtcNow;
            var ticksPlusRand = datetimenow.Ticks + randInt;
            var hashedBytes = algorithm.ComputeHash(BitConverter.GetBytes(ticksPlusRand));
            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        }
    }
}
